#! /bin/sh
#
# start-egd	Script to start and stop egd
#

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/bin/egd.pl
NAME=EGD
DESC="Entropy Gathering Daemon"

test -x $DAEMON || exit 0

set -e

case "$1" in
  start)
	echo -n "Starting $DESC: "
	start-stop-daemon --start --quiet --oknodo \
		--pidfile /var/run/$NAME.pid --exec $DAEMON
	echo "$NAME."
	;;
  stop)
	echo -n "Stopping $DESC: "
	start-stop-daemon --stop --quiet --oknodo \
		--pidfile /var/run/$NAME.pid --exec $DAEMON
	echo "$NAME."
	;;
  reload|force-reload)
	echo "Reloading $DESC configuration files."
	start-stop-daemon --stop --quiet --pidfile \
		/var/run/$NAME.pid --exec $DAEMON
	start-stop-daemon --start --quiet --pidfile \
		/var/run/$NAME.pid --exec $DAEMON
	;;
  restart)
	echo -n "Restarting $DESC: "
	start-stop-daemon --stop --quiet --oknodo --pidfile \
		/var/run/$NAME.pid --exec $DAEMON
	sleep 1
	start-stop-daemon --start --quiet --pidfile \
		/var/run/$NAME.pid --exec $DAEMON
	echo "$NAME."
	;;
  *)
	N=/etc/init.d/start-egd
	echo "Usage: $N {start|stop|restart|reload|force-reload}" >&2
	exit 1
	;;
esac

exit 0
