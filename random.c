/*	random.c PRNG device translator for the Hurd, uses egd.pl

	Copyright (C) 2004 Rian Hunter [rian@thelaststop.net]

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2, or (at
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330,
	Boston, MA	02111-1307	USA */

#define _GNU_SOURCE 1

#include <hurd/trivfs.h>

#include <sys/io.h>	 /* I/O */
#include <sys/mman.h> /* mmap */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <netinet/in.h>

#include <stdlib.h>	 /* exit () */
#include <argp.h>
#include <argz.h>
#include <error.h>		/* Error numers */
#include <fcntl.h>		/* O_READ etc. */
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define MIN(a,b) ((a < b) ? a : b)
#define ARGP_URANDOM 'u'

/* Used by main to communicate with parse_opt. */
struct arguments {
       char *entropy_path; // path to entropy socket
       int urandom;	// if translator should be for urandom
};

static error_t parse_opt (int opt, char *arg, struct argp_state *state);
void connect_to_egd (void);

/* Trivfs hooks. */
int trivfs_fstype = FSTYPE_MISC;	/* Generic trivfs server */
int trivfs_fsid = 0;							/* Should always be 0 on startup */

int trivfs_allow_open = O_READ | O_WRITE;

/* Actual supported modes: */
int trivfs_support_read	= 1;
int trivfs_support_write = 1;
int trivfs_support_exec	= 0;

struct arguments arguments;
int connect_socket;

static const struct argp_option options[] =
{
	{"urandom", ARGP_URANDOM, 0, 0, "Make this a translator for /dev/urandom."},
	{0}
};

static struct argp random_argp = { options, parse_opt, "[egd socket path]", "A random device translator that uses egd.pl (Entropy Gathering Daemon)" };
struct argp *trivfs_runtime_argp = &random_argp;

int main (int argc, char **argv) {
	error_t err;
	mach_port_t bootstrap;
	struct trivfs_control *fsys;



	arguments.urandom = 0;
	arguments.entropy_path = "/var/run/entropy.sock";

	// We use the same argp for options available at startup
	//	 as for options we'll accept in an fsys_set_options RPC.
	argp_parse (&random_argp, argc, argv, 0, 0, &arguments);

	task_get_bootstrap_port (mach_task_self (), &bootstrap);
	if (bootstrap == MACH_PORT_NULL)
		error (1, 0, "Must be started as a translator");

	connect_to_egd();

	// Reply to our parent 
	err = trivfs_startup (bootstrap, 0, 0, 0, 0, 0, &fsys);
	mach_port_deallocate (mach_task_self (), bootstrap);
	if (err)
		error(3, err, "trivfs_startup");

	// Launch.
	ports_manage_port_operations_one_thread(fsys->pi.bucket, trivfs_demuxer, 0);

	return 0;
}

void connect_to_egd (void) {
	struct sockaddr_un server_connect;

	// Initialize Connection to EGD
	memset(&server_connect, 0, sizeof(server_connect));
	server_connect.sun_family		= AF_UNIX;
	strcpy(server_connect.sun_path, arguments.entropy_path);

	connect_socket = socket(AF_UNIX, SOCK_STREAM, 0);
	if (connect_socket == -1) {
		error(1, 0, "Socket Error");
	}

	if (connect(connect_socket, (struct sockaddr *) &server_connect, SUN_LEN(&server_connect)) == -1) {
		error(1, 0, strerror(errno));
	}
}

int entropy_bytes(void) {
	char msg = 0x00;
	int reply;

	if (send(connect_socket, &msg, 1, 0) == -1) {
		error(1, 0, strerror(errno));
	}
		
	if (recv(connect_socket, &reply, 4, 0) == -1) {
		error(1, 0, strerror(errno));
	}
		
	return ntohl(reply)/8;
}

static error_t
parse_opt (int opt, char *arg, struct argp_state *state) {

	struct arguments *arguments = state->input;

	switch (opt) {
	case ARGP_URANDOM:
		arguments->urandom = 1;
		break;
	case ARGP_KEY_ARG:
		if (state->arg_num > 0) {
			// Too many arguments.
			argp_usage (state);
		}
		
		arguments->entropy_path = arg;

		break;
	case ARGP_KEY_END: //do nothing
		break;
	default:
		return ARGP_ERR_UNKNOWN;
		break;
	}

	return 0;
}

/* This will be called from libtrivfs to help construct the answer
	 to an fsys_get_options RPC.	*/
error_t
trivfs_append_args (struct trivfs_control *fsys,
				char **argz, size_t *argz_len) {
	int err = 0;
	if (arguments.urandom == 1) {
		err = argz_add (argz, argz_len, "--urandom");
	}

	return err;
}

/* May do nothing... */
void
trivfs_modify_stat (struct trivfs_protid *cred, io_statbuf_t *st)
{
	/* .. and we do nothing */
}

error_t
trivfs_goaway (struct trivfs_control *cntl, int flags)
{
	exit (EXIT_SUCCESS);
}

/* Read data from an IO object.	If offset is -1, read from the object
	 maintained file pointer.	If the object is not seekable, offset is
	 ignored.	The amount desired to be read is in AMOUNT.	*/
error_t
trivfs_S_io_read (trivfs_protid_t cred,
			mach_port_t reply, mach_msg_type_name_t reply_type,
			data_t *data, mach_msg_type_number_t *data_len,
			loff_t offs, mach_msg_type_number_t amount)
{
	unsigned char msg[2];
	int i;

	/* Deny access if they have bad credentials. */
	if (! cred)
		return EOPNOTSUPP;
	else if (! (cred->po->openmodes & O_READ))
		return EBADF;

	if (amount > 0) {

		msg[0] = (arguments.urandom) ? 0x05 : 0x01;
		msg[1] = MIN(0xFF, amount);

		if (send(connect_socket, msg, 2, 0) == -1) {
			error(1, 0, strerror(errno));
		}

		// get amount of data to read if not urandom
		if (arguments.urandom == 0) {
			if (recv(connect_socket, &msg[1], 1, 0) == -1) {
				error(1, 0, strerror(errno));
			}
		}

		if (msg[1] > 0) {
			// Possibly allocate a new buffer. 
			if (*data_len < msg[1]) {
				*data = mmap (0, msg[1], PROT_READ|PROT_WRITE, MAP_ANON, 0, 0);
				if (*data == MAP_FAILED) {
					// ack we couldn't get enough mem
					// we have to discard the rest of the incoming data
					for (i = 0; i < msg[1]; i++) {
						if (recv(connect_socket, msg, 1, 0) == -1) {
							error(1, 0, strerror(errno));
						}
					}
					return ENOMEM;
				}
			}
			if (recv(connect_socket, *data, msg[1], 0) == -1) {
				error(1, 0, strerror(errno));
			}
		}

		*data_len = msg[1];
	}

	return 0;
}





kern_return_t
trivfs_S_io_write (trivfs_protid_t cred, mach_port_t reply,
			mach_msg_type_name_t replyPoly,	data_t data,
			mach_msg_type_number_t datalen,	loff_t offset,
			vm_size_t *amount)
{
/*	int count = datalen;
	unsigned short bits;
	unsigned char num_of_bytes;
	unsigned char msg[4];
	msg[0] = 0x03;

	if (!cred)
		return EOPNOTSUPP;
	else if (!(cred->po->openmodes & O_WRITE))
		return EBADF;

	while (count > 0) {
		num_of_bytes = MIN(0xFF, count);
		bits = num_of_bytes * 8;
		msg[1] = (bits >> 8) & 0xFF;
		msg[2] = bits & 0xFF;
		msg[3] = num_of_bytes;
		if (send(connect_socket, msg, 4, 0) == -1) {
			error(1, 0, strerror(errno));
		}
		if (send(connect_socket, data, num_of_bytes, 0) == -1) {
			error(1, 0, strerror(errno));
		}
		
		data = ((char *) data) + num_of_bytes;
		count -= num_of_bytes;
	} */

	*amount = datalen;
	return 0;
}

/* Tell how much data can be read from the object without blocking for
	 a "long time" (this should be the same meaning of "long time" used
	 by the nonblocking flag. */
kern_return_t
trivfs_S_io_readable(
	struct trivfs_protid *cred,
	mach_port_t reply,
	mach_msg_type_name_t replytype,
	mach_msg_type_number_t *amount) {

	if (!cred) {
		return EOPNOTSUPP;
	} else if (!(cred->po->openmodes & O_READ)) {
		return EINVAL;
	}

	if (arguments.urandom) {
		*amount = 0xFF; /* Dummy value */
	} else {
		*amount = entropy_bytes();

	}
	
	return 0;
}

/* Truncate file.	*/
kern_return_t
trivfs_S_file_set_size (trivfs_protid_t cred, mach_port_t reply,
			mach_msg_type_name_t replyPoly,	loff_t new_size)
{
	if (!cred)
		return EOPNOTSUPP;
	else
		return 0;
}

/* Change current read/write offset */
error_t
trivfs_S_io_seek (trivfs_protid_t cred, mach_port_t reply,
			mach_msg_type_name_t replyPoly,	loff_t offset,
			int whence, loff_t *newp)
{
	if (! cred)
		return EOPNOTSUPP;
	else
		return 0;
}

/* SELECT_TYPE is the bitwise OR of SELECT_READ, SELECT_WRITE, and
	 SELECT_URG. Block until one of the indicated types of i/o can be
	 done "quickly", and return the types that are then available.
	 TAG is returned as passed; it is just for the convenience of the
	 user in matching up reply messages with specific requests sent. */
kern_return_t
trivfs_S_io_select (trivfs_protid_t cred, mach_port_t reply,
		mach_msg_type_name_t replyPoly,	int *type)
{
	if (!cred)
		return EOPNOTSUPP;
	else
		if (((*type & SELECT_READ) && !(cred->po->openmodes & O_READ))
				|| ((*type & SELECT_WRITE) && !(cred->po->openmodes & O_WRITE)))
			return EBADF;
		else
			*type &= ~SELECT_URG;
	return 0;
}

/* Well, we have to define these four functions, so here we go: */

kern_return_t
trivfs_S_io_get_openmodes (struct trivfs_protid *cred, mach_port_t reply,
													 mach_msg_type_name_t replytype, int *bits)
{
	if (!cred)
		return EOPNOTSUPP;
	else
		{
			*bits = cred->po->openmodes;
			return 0;
		}
}

error_t
trivfs_S_io_set_all_openmodes (struct trivfs_protid *cred,
															 mach_port_t reply,
															 mach_msg_type_name_t replytype,
															 int mode)
{
	if (!cred)
		return EOPNOTSUPP;
	else
		return 0;
}

kern_return_t
trivfs_S_io_set_some_openmodes (struct trivfs_protid *cred,
																mach_port_t reply,
																mach_msg_type_name_t replytype,
																int bits)
{
	if (!cred)
		return EOPNOTSUPP;
	else
		return 0;
}

kern_return_t
trivfs_S_io_clear_some_openmodes (struct trivfs_protid *cred,
																	mach_port_t reply,
																	mach_msg_type_name_t replytype,
																	int bits)
{
	if (!cred)
		return EOPNOTSUPP;
	else
		return 0;
}

