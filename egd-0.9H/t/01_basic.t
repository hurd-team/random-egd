#! /usr/bin/perl -w

use IO::Socket;
use FindBin;
print "1..8\n";

$socket = "test.socket";
$dir = $FindBin::Bin;
$egd = "$dir/../blib/script/egd.pl";

# how do we get at the egd.pl we just built?
$perl = $^X;
@blib = grep {/blib/} @INC;
$inc = join(' ', map { "-I $_" } @blib);
$cmd = "$perl $inc $egd";

print "starting daemon, will die off in 5 minutes or when we kill it\n";
$rc = system("$cmd --quit-after=5 $socket >/dev/null 2>&1");
die "couldn't start egd: $!" if $rc/256;
print "ok 1\n"; $| = 1;

print "sleeping 10 seconds to let the daemon get started..\n";
sleep(10);

$s = new IO::Socket::UNIX('Peer' => $socket);
die "couldn't contact daemon: $!" unless $s;
print "ok 2\n";

{
    # get
    $msg = pack("C", 0x00);
    $s->syswrite($msg, length($msg));
    my $nread = $s->sysread($buf, 4);
    die "didn't read all 4 bytes in one shot" if ($nread != 4);
    my $count = unpack("N",$buf);
    print " $count bits of entropy in pool\n";
    print "ok 3\n";
}

{
    # read
    my $bytes = 10;
    $msg = pack("CC", 0x01, $bytes);
    $s->syswrite($msg, length($msg));
    my $nread = $s->sysread($buf, 1);
    die unless $nread == 1;
    my $count = unpack("C",$buf);
    $nread = $s->sysread($buf, $count);
    die "didn't get all the entropy" unless $nread == $count;
    print " got $count bytes of entropy: ",unpack("H*",$buf),"\n";
    print "ok 4\n";
}

{
    # read (blocking)
    my $bytes = 10;
    $bytes = 255 if $bytes > 255;
    $msg = pack("CC", 0x02, $bytes);
    $s->syswrite($msg, length($msg));
    print " retrieving entropy...\n";
    my $entropy = "";
    while($bytes) {
	my $nread = $s->sysread($buf, $bytes);
	last unless $nread;
	$entropy .= $buf;
	$bytes -= $nread;
	print " ",unpack("H*",$buf),"\n";
    }
    print " done\n";
    print "ok 5\n";
}

{
    # get PID
    $msg = pack("C", 0x04);
    $s->syswrite($msg, length($msg));
    my $nread = $s->sysread($buf, 1);
    die unless $nread == 1;
    my $count = unpack("C", $buf);
    $nread = $s->sysread($buf, $count);
    die "didn't get whole PID string" unless $nread == $count;
    print " pid: $buf\n";
    print "ok 6\n";
}

{
    # bogus command, should drop connection
    my $msg = pack("CC", 0x93, 0x12);
    print " sending bogus message\n";
    $s->syswrite($msg, length($msg));
    sleep(1);
    my $buf;
    my $n = $s->sysread($buf, 10);
    if ($n) {
	print " still connected\n";
    } else {
	print " dropped connection\n";
	print "ok 7\n";
    }
}

# kill daemon
system("$cmd --kill $socket >/dev/null 2>&1");
unlink($socket);
print "ok 8\n";
print " All tests appear successful\n";
