# make the printenv command
#

CFLAGS= -O2 -Wall
LDFLAGS=
CC=gcc
CPP=g++
OBJS=random.o
INC=
LIB=
LIBS=-ltrivfs -lfshelp

all:    random

# To make an executable
random: $(OBJS)
	$(CC) $(LIB) -o $@ $(OBJS) $(LIBS) 

.c.o:
	$(CC) -O3 $(CFLAGS) $(INC) -c $<

clean:
	-rm *.o random

install: random
	cp start-egd.sh /etc/init.d/start-egd.sh
	cp random /hurd/random
	settrans -fcap /dev/random /hurd/random
	settrans -fcap /dev/urandom /hurd/random -u

