#! /bin/sh
### BEGIN INIT INFO
# Provides:          random-egd
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     1 2 3 4 5
# Default-Stop:
# Short-Description: entropy gathering daemon
# Description:       A random translator for the Hurd, and user-space entropy
#                    gathering daemon (EGD).
### END INIT INFO

# Author: Pino Toscano <pino@debian.org>

PATH=/sbin:/bin:/usr/sbin:/usr/bin
DESC="entropy gathering daemon"
NAME=random-egd
DAEMON=/usr/sbin/random-egd
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME

EGD_SOCKET=/var/run/entropy.sock

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# Define LSB log_* functions.
. /lib/lsb/init-functions

#
# Function that starts the daemon/service
#
do_start()
{
	# Return
	#   0 if daemon has been started
	#   1 if daemon was already running
	#   2 if daemon could not be started
	start-stop-daemon --start --quiet --pidfile $PIDFILE --name /usr/bin/perl --startas $DAEMON --test > /dev/null \
		|| return 1
	rm -f $EGD_SOCKET
	settrans -ga /dev/random
	settrans -ga /dev/urandom
	start-stop-daemon --start --quiet --pidfile $PIDFILE --exec $DAEMON -- \
		$EGD_SOCKET \
		|| return 2
	# Add code here, if necessary, that waits for the process to be ready
	# to handle requests from services started subsequently which depend
	# on this one.  As a last resort, sleep for some time.
}

#
# Function that stops the daemon/service
#
do_stop()
{
	# Return
	#   0 if daemon has been stopped
	#   1 if daemon was already stopped
	#   2 if daemon could not be stopped
	#   other if a failure occurred
	start-stop-daemon --stop --quiet --retry=TERM/30/KILL/5 --pidfile $PIDFILE
	RETVAL="$?"
	[ "$RETVAL" = 2 ] && return 2
	# Many daemons don't delete their pidfiles when they exit.
	rm -f $PIDFILE
	rm -f $EGD_SOCKET
	settrans -ga /dev/random
	settrans -ga /dev/urandom
	return "$RETVAL"
}

case "$1" in
  start)
	[ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
	do_start
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  stop)
	[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
	do_stop
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  status)
       status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
       ;;
  restart|force-reload)
	#
	# If the "reload" option is implemented then remove the
	# 'force-reload' alias
	#
	log_daemon_msg "Restarting $DESC" "$NAME"
	do_stop
	case "$?" in
	  0|1)
		do_start
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;; # Old process is still running
			*) log_end_msg 1 ;; # Failed to start
		esac
		;;
	  *)
	  	# Failed to stop
		log_end_msg 1
		;;
	esac
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|status|restart|force-reload}" >&2
	exit 3
	;;
esac

:
